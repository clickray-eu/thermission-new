function mobileMenuInit() {
    if($(".header-container-wrapper .widget-type-menu .hs-menu-wrapper > ul").length>0){
        $(".header-container-wrapper .widget-type-menu .hs-menu-wrapper > ul").slicknav({
            label: "",
            allowParentLinks: true,
            init:function(){
                var logo = $('#hs-link-logo img').clone().addClass('mobile-logo');
                $('.slicknav_menu').prepend(logo);
                $('.slicknav_menu .slicknav_btn').append('<i class="fa fa-bars" aria-hidden="true"></i>');

                var search=$('.second-menu .search').clone(true);
                $(".slicknav_nav").append("<li class='mobile-search'></li>");
                $(".slicknav_nav .mobile-search").append(search);

                var language=$('.secound-menu-languages').clone(true);
                $(".slicknav_nav").append("<li class='mobile-language'></li>");
                $(".slicknav_nav .mobile-language").append(language);
            },
            beforeOpen:function(event){
                if(event.hasClass('slicknav_btn')) {
                    $('.slicknav_menu .slicknav_btn i').removeClass('fa-bars').addClass('fa-times');
                    $('body').css('overflow', 'hidden').prepend('<div class="mobile-menu-overlay"></div>');
                    $('.mobile-menu-overlay').animate({opacity: 0.7}, 300);
                }
            },
            afterClose:function(event){
                if(event.hasClass('slicknav_btn')) {
                    $('.slicknav_menu .slicknav_btn i').removeClass('fa-times').addClass('fa-bars');
                    $('body').css('overflow', 'scroll');
                    $('.mobile-menu-overlay').animate({opacity: 0}, 250);
                    setTimeout(function(){
                        $('.mobile-menu-overlay').remove();
                    }, 250);
                }
            }
        });
    }
}

$(document).ready(function() {
    mobileMenuInit();
    $(".scrolled-menu").sticky({topSpacing:0});

    if ("ontouchstart" in window || navigator.msMaxTouchPoints) {
        $('.header-container-wrapper .widget-type-menu .hs-menu-depth-1.hs-item-has-children > a').on('click', function(e) {
          e.preventDefault();
        })
    }
});
