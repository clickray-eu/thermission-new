$(window).load(function() {
	var contactEmail = $('#email-prefs-form input[name="email"]').val();
	$('input[name="email"].hs-input').each(function() {
		$(this).val(contactEmail);
	});

    $('.data-form form select').on('change', function () {
        $('form select').css('color', '#474747');
    });
});