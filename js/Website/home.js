$(document).ready(function() {
    // Location hash in contact page
    console.log(location.hash);


	// Hero Slider + progressbar
	var time = 5;
  	var isPause,
    	tick,
    	percentTime;

    var $slick = $('.home .slider-content > span');
    	$slick.slick({
    	speed: 1200,
		fade: true,
		dots: true,
		arrows: false,
		infinite: true,
		slidesToShow: 1
	});

	$(".slick-list").parent().append('<div class="progress"></div>');
  	var $bar = $('.progress');
	$('.slider-content .slick-list').on({
	    mouseenter: function() {
	      isPause = true;
	    },
	    mouseleave: function() {
	      isPause = false;
	    }
  	});

	function startProgressbar() {
    	resetProgressbar();
    	percentTime = 0;
    	isPause = false;
    	tick = setInterval(interval, 10);
  	}

	function interval() {
    	if(isPause === false) {
      		percentTime += 1 / (time+0.1);
      		$bar.css({
        		width: percentTime+"%"
      		});
    	if(percentTime >= 100)
        	{
          		$slick.slick('slickNext');
          		startProgressbar();
        	}
    	}
 	}

	function resetProgressbar() {
    	$bar.css({
    		width: 0+'%'
    	});
    	clearTimeout(tick);
  	}

	startProgressbar();


	// Hide and Show Slick Dots
	$( ".slider-content .slick-list, .home .slider-content .slick-dots " ).on({
	  mouseenter: function() {
	    $(".home .slider-content .slick-dots").css("opacity", "1");
	  },
	  mouseleave: function() {
	    $(".slider-content .slick-dots").css("opacity", "0");
	  }
	});


 });