function waitForLoad(wrapper, element, callback) {
 if ($(wrapper).length > 0) {
     $(wrapper).each(function(i, el) {
       var waitForLoad = setInterval(function() {
       if ($(el).length == $(el).find(element).length) {
          clearInterval(waitForLoad);
          callback($(el), $(el).find(element));
       }
     }, 50);
 });
 }
}
waitForLoad(".second-menu .search","form",function(wrapper,element){ 
		wrapper.find('.search-btn').click(function(e){
			e.stopPropagation();
			wrapper.find('.search-form').css('z-index',999999);		
			wrapper.find('.search-form').addClass("active");
		});
		wrapper.find('.search-form').click(function(e){
			e.stopPropagation();
		});
		$("body").click(function(e){	
			wrapper.find('.search-form').css('z-index',999999);		
			wrapper.find('.search-form').removeClass("active");
			setTimeout(function(){
				wrapper.find('.search-form').css('z-index',-1);		
			},300);
		});

 });