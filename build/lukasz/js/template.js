'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// file: event.js
// Sticky Plugin v1.0.4 for jQuery
// =============
// Author: Anthony Garand
// Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk)
// Improvements by Leonardo C. Daronco (daronco)
// Created: 02/14/2011
// Date: 07/20/2015
// Website: http://stickyjs.com/
// Description: Makes an element on the page stick on the screen as you scroll
//              It will only set the 'top' and 'position' of your element, you
//              might need to adjust the width in some cases.

(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if ((typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals
    factory(jQuery);
  }
})(function ($) {
  var slice = Array.prototype.slice; // save ref to original slice()
  var splice = Array.prototype.splice; // save ref to original slice()

  var defaults = {
    topSpacing: 0,
    bottomSpacing: 0,
    className: 'is-sticky',
    wrapperClassName: 'sticky-wrapper',
    center: false,
    getWidthFrom: '',
    widthFromWrapper: true, // works only when .getWidthFrom is empty
    responsiveWidth: false,
    zIndex: 'auto'
  },
      $window = $(window),
      $document = $(document),
      sticked = [],
      windowHeight = $window.height(),
      scroller = function scroller() {
    var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = scrollTop > dwh ? dwh - scrollTop : 0;

    for (var i = 0, l = sticked.length; i < l; i++) {
      var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

      //update height in case of dynamic content
      s.stickyWrapper.css('height', s.stickyElement.outerHeight());

      if (scrollTop <= etse) {
        if (s.currentTop !== null) {
          s.stickyElement.css({
            'width': '',
            'position': '',
            'top': '',
            'z-index': ''
          });
          s.stickyElement.parent().removeClass(s.className);
          s.stickyElement.trigger('sticky-end', [s]);
          s.currentTop = null;
        }
      } else {
        var newTop = documentHeight - s.stickyElement.outerHeight() - s.topSpacing - s.bottomSpacing - scrollTop - extra;
        if (newTop < 0) {
          newTop = newTop + s.topSpacing;
        } else {
          newTop = s.topSpacing;
        }
        if (s.currentTop !== newTop) {
          var newWidth;
          if (s.getWidthFrom) {
            newWidth = $(s.getWidthFrom).width() || null;
          } else if (s.widthFromWrapper) {
            newWidth = s.stickyWrapper.width();
          }
          if (newWidth == null) {
            newWidth = s.stickyElement.width();
          }
          s.stickyElement.css('width', newWidth).css('position', 'fixed').css('top', newTop).css('z-index', s.zIndex);

          s.stickyElement.parent().addClass(s.className);

          if (s.currentTop === null) {
            s.stickyElement.trigger('sticky-start', [s]);
          } else {
            // sticky is started but it have to be repositioned
            s.stickyElement.trigger('sticky-update', [s]);
          }

          if (s.currentTop === s.topSpacing && s.currentTop > newTop || s.currentTop === null && newTop < s.topSpacing) {
            // just reached bottom || just started to stick but bottom is already reached
            s.stickyElement.trigger('sticky-bottom-reached', [s]);
          } else if (s.currentTop !== null && newTop === s.topSpacing && s.currentTop < newTop) {
            // sticky is started && sticked at topSpacing && overflowing from top just finished
            s.stickyElement.trigger('sticky-bottom-unreached', [s]);
          }

          s.currentTop = newTop;
        }

        // Check if sticky has reached end of container and stop sticking
        var stickyWrapperContainer = s.stickyWrapper.parent();
        var unstick = s.stickyElement.offset().top + s.stickyElement.outerHeight() >= stickyWrapperContainer.offset().top + stickyWrapperContainer.outerHeight() && s.stickyElement.offset().top <= s.topSpacing;

        if (unstick) {
          s.stickyElement.css('position', 'absolute').css('top', '').css('bottom', 0).css('z-index', '');
        } else {
          s.stickyElement.css('position', 'fixed').css('top', newTop).css('bottom', '').css('z-index', s.zIndex);
        }
      }
    }
  },
      resizer = function resizer() {
    windowHeight = $window.height();

    for (var i = 0, l = sticked.length; i < l; i++) {
      var s = sticked[i];
      var newWidth = null;
      if (s.getWidthFrom) {
        if (s.responsiveWidth) {
          newWidth = $(s.getWidthFrom).width();
        }
      } else if (s.widthFromWrapper) {
        newWidth = s.stickyWrapper.width();
      }
      if (newWidth != null) {
        s.stickyElement.css('width', newWidth);
      }
    }
  },
      methods = {
    init: function init(options) {
      return this.each(function () {
        var o = $.extend({}, defaults, options);
        var stickyElement = $(this);

        var stickyId = stickyElement.attr('id');
        var wrapperId = stickyId ? stickyId + '-' + defaults.wrapperClassName : defaults.wrapperClassName;
        var wrapper = $('<div></div>').attr('id', wrapperId).addClass(o.wrapperClassName);

        stickyElement.wrapAll(function () {
          if ($(this).parent("#" + wrapperId).length == 0) {
            return wrapper;
          }
        });

        var stickyWrapper = stickyElement.parent();

        if (o.center) {
          stickyWrapper.css({ width: stickyElement.outerWidth(), marginLeft: "auto", marginRight: "auto" });
        }

        if (stickyElement.css("float") === "right") {
          stickyElement.css({ "float": "none" }).parent().css({ "float": "right" });
        }

        o.stickyElement = stickyElement;
        o.stickyWrapper = stickyWrapper;
        o.currentTop = null;

        sticked.push(o);

        methods.setWrapperHeight(this);
        methods.setupChangeListeners(this);
      });
    },

    setWrapperHeight: function setWrapperHeight(stickyElement) {
      var element = $(stickyElement);
      var stickyWrapper = element.parent();
      if (stickyWrapper) {
        stickyWrapper.css('height', element.outerHeight());
      }
    },

    setupChangeListeners: function setupChangeListeners(stickyElement) {
      if (window.MutationObserver) {
        var mutationObserver = new window.MutationObserver(function (mutations) {
          if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) {
            methods.setWrapperHeight(stickyElement);
          }
        });
        mutationObserver.observe(stickyElement, { subtree: true, childList: true });
      } else {
        if (window.addEventListener) {
          stickyElement.addEventListener('DOMNodeInserted', function () {
            methods.setWrapperHeight(stickyElement);
          }, false);
          stickyElement.addEventListener('DOMNodeRemoved', function () {
            methods.setWrapperHeight(stickyElement);
          }, false);
        } else if (window.attachEvent) {
          stickyElement.attachEvent('onDOMNodeInserted', function () {
            methods.setWrapperHeight(stickyElement);
          });
          stickyElement.attachEvent('onDOMNodeRemoved', function () {
            methods.setWrapperHeight(stickyElement);
          });
        }
      }
    },
    update: scroller,
    unstick: function unstick(options) {
      return this.each(function () {
        var that = this;
        var unstickyElement = $(that);

        var removeIdx = -1;
        var i = sticked.length;
        while (i-- > 0) {
          if (sticked[i].stickyElement.get(0) === that) {
            splice.call(sticked, i, 1);
            removeIdx = i;
          }
        }
        if (removeIdx !== -1) {
          unstickyElement.unwrap();
          unstickyElement.css({
            'width': '',
            'position': '',
            'top': '',
            'float': '',
            'z-index': ''
          });
        }
      });
    }
  };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener('scroll', scroller, false);
    window.addEventListener('resize', resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent('onscroll', scroller);
    window.attachEvent('onresize', resizer);
  }

  $.fn.sticky = function (method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if ((typeof method === 'undefined' ? 'undefined' : _typeof(method)) === 'object' || !method) {
      return methods.init.apply(this, arguments);
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };

  $.fn.unstick = function (method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if ((typeof method === 'undefined' ? 'undefined' : _typeof(method)) === 'object' || !method) {
      return methods.unstick.apply(this, arguments);
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };
  $(function () {
    setTimeout(scroller, 0);
  });
});

// end file: event.js

// file: global.js
function mobileMenuInit() {
  if ($(".header-container-wrapper .widget-type-menu .hs-menu-wrapper > ul").length > 0) {
    $(".header-container-wrapper .widget-type-menu .hs-menu-wrapper > ul").slicknav({
      label: "",
      allowParentLinks: true,
      init: function init() {
        var logo = $('#hs-link-logo img').clone().addClass('mobile-logo');
        $('.slicknav_menu').prepend(logo);
        $('.slicknav_menu .slicknav_btn').append('<i class="fa fa-bars" aria-hidden="true"></i>');

        var search = $('.second-menu .search').clone(true);
        $(".slicknav_nav").append("<li class='mobile-search'></li>");
        $(".slicknav_nav .mobile-search").append(search);

        var language = $('.secound-menu-languages').clone(true);
        $(".slicknav_nav").append("<li class='mobile-language'></li>");
        $(".slicknav_nav .mobile-language").append(language);
      },
      beforeOpen: function beforeOpen(event) {
        if (event.hasClass('slicknav_btn')) {
          $('.slicknav_menu .slicknav_btn i').removeClass('fa-bars').addClass('fa-times');
          $('body').css('overflow', 'hidden').prepend('<div class="mobile-menu-overlay"></div>');
          $('.mobile-menu-overlay').animate({ opacity: 0.7 }, 300);
        }
      },
      afterClose: function afterClose(event) {
        if (event.hasClass('slicknav_btn')) {
          $('.slicknav_menu .slicknav_btn i').removeClass('fa-times').addClass('fa-bars');
          $('body').css('overflow', 'scroll');
          $('.mobile-menu-overlay').animate({ opacity: 0 }, 250);
          setTimeout(function () {
            $('.mobile-menu-overlay').remove();
          }, 250);
        }
      }
    });
  }
}

$(document).ready(function () {
  mobileMenuInit();
  $(".scrolled-menu").sticky({ topSpacing: 0 });

  if ("ontouchstart" in window || navigator.msMaxTouchPoints) {
    $('.header-container-wrapper .widget-type-menu .hs-menu-depth-1.hs-item-has-children > a').on('click', function (e) {
      e.preventDefault();
    });
  }
});

// end file: global.js

// file: Modules/search-module.js
function waitForLoad(wrapper, element, callback) {
  if ($(wrapper).length > 0) {
    $(wrapper).each(function (i, el) {
      var waitForLoad = setInterval(function () {
        if ($(el).length == $(el).find(element).length) {
          clearInterval(waitForLoad);
          callback($(el), $(el).find(element));
        }
      }, 50);
    });
  }
}
waitForLoad(".second-menu .search", "form", function (wrapper, element) {
  wrapper.find('.search-btn').click(function (e) {
    e.stopPropagation();
    wrapper.find('.search-form').css('z-index', 999999);
    wrapper.find('.search-form').addClass("active");
  });
  wrapper.find('.search-form').click(function (e) {
    e.stopPropagation();
  });
  $("body").click(function (e) {
    wrapper.find('.search-form').css('z-index', 999999);
    wrapper.find('.search-form').removeClass("active");
    setTimeout(function () {
      wrapper.find('.search-form').css('z-index', -1);
    }, 300);
  });
});
// end file: Modules/search-module.js

// file: Website/home.js
$(document).ready(function () {
  // Location hash in contact page
  console.log(location.hash);

  // Hero Slider + progressbar
  var time = 5;
  var isPause, tick, percentTime;

  var $slick = $('.home .slider-content > span');
  $slick.slick({
    speed: 1200,
    fade: true,
    dots: true,
    arrows: false,
    infinite: true,
    slidesToShow: 1
  });

  $(".slick-list").parent().append('<div class="progress"></div>');
  var $bar = $('.progress');
  $('.slider-content .slick-list').on({
    mouseenter: function mouseenter() {
      isPause = true;
    },
    mouseleave: function mouseleave() {
      isPause = false;
    }
  });

  function startProgressbar() {
    resetProgressbar();
    percentTime = 0;
    isPause = false;
    tick = setInterval(interval, 10);
  }

  function interval() {
    if (isPause === false) {
      percentTime += 1 / (time + 0.1);
      $bar.css({
        width: percentTime + "%"
      });
      if (percentTime >= 100) {
        $slick.slick('slickNext');
        startProgressbar();
      }
    }
  }

  function resetProgressbar() {
    $bar.css({
      width: 0 + '%'
    });
    clearTimeout(tick);
  }

  startProgressbar();

  // Hide and Show Slick Dots
  $(".slider-content .slick-list, .home .slider-content .slick-dots ").on({
    mouseenter: function mouseenter() {
      $(".home .slider-content .slick-dots").css("opacity", "1");
    },
    mouseleave: function mouseleave() {
      $(".slider-content .slick-dots").css("opacity", "0");
    }
  });
});
// end file: Website/home.js

// file: Website/preferences-page.js
$(window).load(function () {
  var contactEmail = $('#email-prefs-form input[name="email"]').val();
  $('input[name="email"].hs-input').each(function () {
    $(this).val(contactEmail);
  });

  $('.data-form form select').on('change', function () {
    $('form select').css('color', '#474747');
  });
});
// end file: Website/preferences-page.js
//# sourceMappingURL=template.js.map
